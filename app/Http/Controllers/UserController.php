<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return response()->json(compact('token'));
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_lengkap' => 'required|string|max:255',

            'email' => 'required|string|email|max:255|unique:users',
             'no_hp' => 'required|string|max:255',
              'password' => 'required|string|min:6',
              'alamat' => 'required|string|max:255',
               'prov' => 'required|string|max:255',
             'kabupaten_kota' => 'required|string|max:255'     
           
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = User::create([
            'nama_lengkap' => $request->get('nama_lengkap'),
            'email' => $request->get('email'),
            'no_hp' => $request->get('no_hp'),
            'password' => Hash::make($request->get('password')),
             'alamat'=>$request->get('alamat'),
            'prov'=> $request->get('prov'),
            'kabupaten_kota'=>$request->get('kabupaten_kota')
        ]);

        $token = JWTAuth::fromUser($user);

        return response()->json(compact('user','token'),201);
    }

    public function getAuthenticatedUser()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        return response()->json(compact('user'));
    }
      public function update(Request $request, $id)
    {


    $nama_lengkap = $request->nama_lengkap;
    $email = $request->email;
    $no_hp = $request->no_hp;
    $alamat = $request->alamat;
    $prov = $request->prov;
    $kabupaten_kota = $request->kabupaten_kota;
    $user = User::find($id);
    $user->nama_lengkap = $nama_lengkap;
    $user->email = $email;
    $user->no_hp = $no_hp;
    $user->alamat = $alamat;
    $user->prov = $prov;
    $user->kabupaten_kota = $kabupaten_kota;
    $success = $user->save();
 
    if(!$success)
    {
             return Response()->json("error updating",500);
    }else{
 
        return Response()->json("success",201);

}
}     
}