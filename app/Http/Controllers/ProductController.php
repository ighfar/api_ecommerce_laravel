<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    
      public function index() {
        return Product::all();
    }

      public function detail($id)
    {
        $product = Product::whereId($id)->first();


        if ($product) {
            return response()->json([
                'success' => true,
                'message' => 'Detail Product!',
                'data'    => $product
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Product Tidak Ditemukan!',
                'data'    => ''
            ], 401);
        }
    }

    
}
