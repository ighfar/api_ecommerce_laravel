<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
      protected $table = 'orders';
   protected $guarded = ['id']; 

 function product(){
      return $this->hasMany('App\Product','order_id','id');
    
}

 function product(){
      return $this->hasMany('App\User','order_id','id');
    
}

   protected $casts = [
   		'ekspedisi' => 'array',
   	];
}
